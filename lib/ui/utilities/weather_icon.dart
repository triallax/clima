/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/// Returns the weather icon that corresponds to the given [weatherCode].
String getWeatherIcon(int weatherCode, {required bool isDay}) {
  // See table at bottom of https://open-meteo.com/en/docs.
  switch (weatherCode) {
    case 0: // Clear sky
    case 1: // Mainly clear
      return isDay ? 'assets/day.svg' : 'assets/night.svg';

    case 2: // Partly cloudy
      return isDay ? 'assets/cloudy_day.svg' : 'assets/cloudy_night.svg';

    case 3: // Overcast
      return 'assets/cloudy.svg';

    case 45: // Fog
    case 48: // Depositing rime fog
      return 'assets/foggy.svg';

    case 51: // Light drizzle
      return 'assets/rainy_light.svg';
    case 53: // Moderate drizzle
      return 'assets/rainy_moderate.svg';
    case 55: // Dense drizzle
      return 'assets/rainy_heavy.svg';
    case 56: // Light freezing drizzle
      return 'assets/rainy_light.svg';
    case 57: // Dense freezing drizzle
      return 'assets/rainy_freezing.svg';

    case 61: // Light rain
      return 'assets/rainy_light.svg';
    case 63: // Moderate rain
      return 'assets/rainy_moderate.svg';
    case 65: // Heavy rain
      return 'assets/rainy_heavy.svg';
    case 66: // Light freezing rain
      return 'assets/rainy_light.svg';
    case 67: // Heavy freezing rain
      return 'assets/rainy_freezing.svg';

    case 71: // Light snow fall
      return 'assets/snowy_light.svg';
    case 73: // Moderate snow fall
      return 'assets/snowy_moderate.svg';
    case 75: // Heavy snow fall
      return 'assets/snowy_heavy.svg';
    case 77: // Snow grains
      return 'assets/snowy_light.svg';

    case 80: // Light rain showers
      return 'assets/rainy_light.svg';
    case 81: // Moderate rain showers
      return 'assets/rainy_moderate.svg';
    case 82: // Violent rain showers
      return 'assets/rainy_heavy.svg';
    case 85: // Light snow showers
      return 'assets/snowy_moderate.svg';
    case 86: // Heavy snow showers
      return 'assets/snowy_heavy.svg';

    case 95: // Light or moderate thunderstorm
    case 96: // Thunderstorm with light hail
    case 99: // Thunderstorm with heavy hail
      return 'assets/thunder.svg';

    default:
      throw ArgumentError('Failed to map weather code $weatherCode to icon');
  }
}
