/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/core/failure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void showFailureSnackBar(
  BuildContext context, {
  required Failure failure,
  VoidCallback? onRetry,
  int? duration,
}) {
  final appLocalizations = AppLocalizations.of(context)!;

  final text = () {
    if (failure is NoConnection) {
      return appLocalizations.weather_failureSnackbar_noConnection;
    } else if (failure is FailedToParseResponse) {
      return appLocalizations.weather_failureSnackbar_failedToParseResponse;
    } else if (failure is ServerDown) {
      return appLocalizations.weather_failureSnackbar_cannotConnectToServer;
    } else if (failure is InvalidCityName) {
      return appLocalizations.weather_failureSnackbar_invalidCityName;
    } else if (failure is ServerError) {
      if (failure.reason != null) {
        return appLocalizations
            .weather_failureSnackbar_serverError(failure.reason!);
      } else {
        return appLocalizations.weather_failureSnackbar_serverErrorUnspecified;
      }
    } else {
      throw ArgumentError('Did not expect $failure');
    }
  }();

  showSnackBar(
    context,
    text: text,
    actionText: appLocalizations.weather_failureSnackbar_retry,
    onPressed: onRetry,
    duration: duration,
  );
}

void showSnackBar(
  BuildContext context, {
  required String text,
  String? actionText,
  VoidCallback? onPressed,
  int? duration,
}) {
  final messenger = ScaffoldMessenger.of(context);
  messenger.removeCurrentSnackBar();
  messenger.showSnackBar(
    SnackBar(
      elevation: 0,
      behavior: SnackBarBehavior.floating,
      content: Text(text),
      duration: Duration(seconds: duration ?? 4),
      action: onPressed != null
          ? SnackBarAction(
              label: actionText!,
              textColor: Theme.of(context).colorScheme.secondary,
              onPressed: onPressed,
            )
          : null,
    ),
  );
}
