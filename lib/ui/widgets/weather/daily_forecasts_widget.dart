/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/ui/state_notifiers/full_weather_state_notifier.dart' as w;
import 'package:clima/ui/utilities/constants.dart';
import 'package:clima/ui/utilities/weather_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';

class DailyForecastsWidget extends ConsumerWidget {
  const DailyForecastsWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context)!;

    final dailyForecasts = ref.watch(
      w.fullWeatherStateNotifierProvider.select(
        (state) => state.fullWeather!.dailyForecasts,
      ),
    );
    final currentDayForecast = ref.watch(
      w.fullWeatherStateNotifierProvider.select(
        (state) => state.fullWeather!.currentDayForecast,
      ),
    );

    return Column(
      children: [
        for (final dailyForecast in dailyForecasts)
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    dailyForecast == currentDayForecast
                        ? appLocalizations.weather_today
                        : DateFormat.EEEE().format(dailyForecast.date),
                    style: kSubtitle1TextStyle(context).copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        getWeatherIcon(dailyForecast.weatherCode, isDay: true),
                        height: 7.h,
                      ),
                      // We are supposed to use `FaIcon` with Font Awesome
                      // icons, but in this case it makes the icon too close to
                      // the text. It also makes the icon a bit larger.
                      // See https://codeberg.org/Lacerte/clima/issues/351.
                      Icon(
                        FontAwesomeIcons.droplet,
                        color: Theme.of(context).textTheme.titleSmall!.color,
                        size: kIconSize(context),
                      ),
                      Text(
                        '${dailyForecast.pop.round()}%',
                        style: kSubtitle2TextStyle(context),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${dailyForecast.maxTemperature.round()}°',
                        style: kSubtitle1TextStyle(context),
                      ),
                      Text(
                        '/',
                        style: kSubtitle2TextStyle(context),
                      ),
                      Text(
                        '${dailyForecast.minTemperature.round()}°',
                        style: kSubtitle2TextStyle(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }
}
