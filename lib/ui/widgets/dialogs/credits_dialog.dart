/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/ui/widgets/others/link_text_span.dart';
import 'package:flutter/material.dart';

class CreditsDialog extends StatelessWidget {
  const CreditsDialog({super.key});

  @override
  Widget build(BuildContext context) {
    final textSpanStyle = TextStyle(
      color: Theme.of(context).textTheme.titleMedium!.color,
    );
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(24),
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "The app's weather data is provided by ",
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'Open-Meteo',
                    url: 'https://open-meteo.com',
                  ),
                  TextSpan(text: ' under ', style: textSpanStyle),
                  linkTextSpan(
                    context: context,
                    text: 'CC BY 4.0',
                    url: 'https://creativecommons.org/licenses/by/4.0/',
                  ),
                  TextSpan(
                      text: ". Open-Meteo's data sources are documented ",
                      style: textSpanStyle),
                  linkTextSpan(
                      context: context,
                      text: 'here',
                      url: 'https://open-meteo.com/en/license'),
                  TextSpan(text: '.', style: textSpanStyle),
                ],
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "The app's geocoding data is provided by ",
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'Open-Meteo',
                    url: 'https://open-meteo.com',
                  ),
                  TextSpan(text: ' under ', style: textSpanStyle),
                  linkTextSpan(
                    context: context,
                    text: 'CC BY 4.0',
                    url: 'https://creativecommons.org/licenses/by/4.0/',
                  ),
                  TextSpan(text: ', based on data from ', style: textSpanStyle),
                  linkTextSpan(
                      context: context,
                      text: 'GeoNames',
                      url: 'https://www.geonames.org/'),
                  TextSpan(text: '.', style: textSpanStyle),
                ],
              ),
            ),
            Divider(
              color:
                  Theme.of(context).textTheme.titleMedium!.color!.withAlpha(65),
            ),
            RichText(
              text: TextSpan(
                text: "The app logo's ",
                style: textSpanStyle,
                children: [
                  linkTextSpan(
                    context: context,
                    text: 'icon',
                    url: 'https://www.iconfinder.com/iconsets/tiny-weather-1',
                  ),
                  TextSpan(
                    text: ' is designed by ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'Paolo Spot Valzania',
                    url: 'https://linktr.e/paolospotvalzania',
                  ),
                  TextSpan(
                    text: ', licensed under ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'CC BY 3.0',
                    url: 'https://creativecommons.org/licenses/by/3.0/',
                  ),
                  TextSpan(
                    text: ' / Placed on top of a light blue background.',
                    style: textSpanStyle,
                  ),
                ],
              ),
            ),
            Divider(
              color:
                  Theme.of(context).textTheme.titleMedium!.color!.withAlpha(65),
            ),
            RichText(
              text: TextSpan(
                text: 'The ',
                style: textSpanStyle,
                children: [
                  linkTextSpan(
                    context: context,
                    text: 'weather icons',
                    url:
                        'https://www.amcharts.com/free-animated-svg-weather-icons/',
                  ),
                  TextSpan(
                    text:
                        ' used inside the app (except for the "foggy" icon, see next entry) are designed by ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'amCharts',
                    url: 'https://www.amcharts.com/',
                  ),
                  TextSpan(
                    text: ' and licensed under ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'CC BY 4.0',
                    url: 'https://creativecommons.org/licenses/by/4.0/',
                  ),
                  TextSpan(text: '.', style: textSpanStyle),
                ],
              ),
            ),
            Divider(
              color:
                  Theme.of(context).textTheme.titleMedium!.color!.withAlpha(65),
            ),
            RichText(
              text: TextSpan(
                text: 'The ',
                style: textSpanStyle,
                children: [
                  linkTextSpan(
                    context: context,
                    text: '"foggy" weather icon',
                    url:
                        'https://www.amcharts.com/free-animated-svg-weather-icons/',
                  ),
                  TextSpan(
                    text: ' used inside the app is part of ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'Material Symbols',
                    url: 'https://fonts.google.com/icons',
                  ),
                  TextSpan(
                    text: ' and licensed under the ',
                    style: textSpanStyle,
                  ),
                  linkTextSpan(
                    context: context,
                    text: 'Apache License, version 2.0',
                    url: 'https://www.apache.org/licenses/LICENSE-2.0.html',
                  ),
                  TextSpan(
                    text: ' / Icon color changed to light blue.',
                    style: textSpanStyle,
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
