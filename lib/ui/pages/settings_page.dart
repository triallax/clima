/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/data/models/dark_theme_model.dart';
import 'package:clima/data/models/theme_model.dart';
import 'package:clima/domain/entities/unit_system.dart';
import 'package:clima/ui/pages/about_clima_page.dart';
import 'package:clima/ui/state_notifiers/theme_state_notifier.dart';
import 'package:clima/ui/state_notifiers/unit_system_state_notifier.dart'
    hide Error;
import 'package:clima/ui/widgets/dialogs/dark_theme_dialog.dart';
import 'package:clima/ui/widgets/dialogs/theme_dialog.dart';
import 'package:clima/ui/widgets/dialogs/unit_system_dialog.dart';
import 'package:clima/ui/widgets/settings/settings_divider.dart';
import 'package:clima/ui/widgets/settings/settings_header.dart';
import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class SettingsPage extends ConsumerWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appLocalizations = AppLocalizations.of(context)!;

    final theme =
        ref.watch(themeStateNotifierProvider.select((state) => state.theme));
    final darkTheme = ref.watch(
      themeStateNotifierProvider.select((state) => state.darkTheme),
    );

    final unitSystem = ref.watch(
      unitSystemStateNotifierProvider.select((state) => state.unitSystem),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          appLocalizations.settings_title,
          style: TextStyle(
            color: Theme.of(context).appBarTheme.titleTextStyle!.color,
            fontSize: Theme.of(context).textTheme.titleLarge!.fontSize,
          ),
        ),
      ),
      body: unitSystem == null
          ? null
          : SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SettingsHeader(title: appLocalizations.settings_general),
                  SettingsTile(
                    title: appLocalizations.settings_unitSystem,
                    subtitle: () {
                      switch (unitSystem) {
                        case UnitSystem.metric:
                          return appLocalizations.settings_unitSystem_metric;

                        case UnitSystem.imperial:
                          return appLocalizations.settings_unitSystem_imperial;
                      }
                    }(),
                    leading: Icon(
                      Icons.straighten_outlined,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    onTap: () => showDialog<void>(
                      context: context,
                      builder: (context) => const UnitSystemDialog(),
                    ),
                  ),
                  const SettingsDivider(),
                  SettingsHeader(title: appLocalizations.settings_interface),
                  SettingsTile(
                    title: appLocalizations.settings_theme,
                    subtitle: () {
                      switch (theme) {
                        case ThemeModel.light:
                          return appLocalizations.settings_theme_light;

                        case ThemeModel.dark:
                          return appLocalizations.settings_theme_dark;

                        case ThemeModel.systemDefault:
                          return appLocalizations.settings_theme_systemDefault;

                        default:
                          throw Error();
                      }
                    }(),
                    padding: 80.0,
                    onTap: () => showDialog<void>(
                      context: context,
                      builder: (context) => const ThemeDialog(),
                    ),
                  ),
                  SettingsTile(
                    title: 'Dark theme',
                    subtitle: () {
                      switch (darkTheme) {
                        case DarkThemeModel.darkGrey:
                          return appLocalizations.settings_darkTheme_default;

                        case DarkThemeModel.black:
                          return appLocalizations.settings_darkTheme_black;

                        default:
                          throw Error();
                      }
                    }(),
                    padding: 80.0,
                    onTap: () => showDialog<void>(
                      context: context,
                      builder: (context) => const DarkThemeDialog(),
                    ),
                  ),
                  const SettingsDivider(),
                  SettingsHeader(title: appLocalizations.settings_about),
                  SettingsTile(
                    title: appLocalizations.settings_aboutClima,
                    leading: Icon(
                      Icons.info_outline,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (context) => const AboutClimaPage(),
                      ),
                    ),
                  ),
                  const SettingsDivider(),
                ],
              ),
            ),
    );
  }
}
