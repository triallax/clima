/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import 'package:clima/constants.dart';
import 'package:clima/ui/build_flavor.dart';
import 'package:clima/ui/widgets/dialogs/credits_dialog.dart';
import 'package:clima/ui/widgets/dialogs/help_and_feedback_dialog.dart';
import 'package:clima/ui/widgets/settings/settings_divider.dart';
import 'package:clima/ui/widgets/settings/settings_header.dart';
import 'package:clima/ui/widgets/settings/settings_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutClimaPage extends StatelessWidget {
  const AboutClimaPage({super.key});

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context)!;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          appLocalizations.aboutClima_title,
          style: TextStyle(
            color: Theme.of(context).appBarTheme.titleTextStyle!.color,
            fontSize: Theme.of(context).textTheme.titleLarge!.fontSize,
          ),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SettingsHeader(title: appLocalizations.aboutClima_information),
            SettingsTile(
              title: appLocalizations.aboutClima_changelog,
              subtitle:
                  appLocalizations.aboutClima_changelog_subtitle(appVersion),
              leading: Icon(
                Icons.new_releases_outlined,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () => launchUrl(
                Uri.parse(
                  'https://codeberg.org/lacerte/clima/releases/tag/v$appVersion',
                ),
              ),
            ),
            // Google doesn't like donate buttons apparently. Stupid, I know.
            // Example: https://github.com/streetcomplete/StreetComplete/issues/3768
            if (buildFlavor != BuildFlavor.googlePlay)
              SettingsTile(
                title: appLocalizations.aboutClima_donate,
                subtitle: appLocalizations.aboutClima_donate_subtitle,
                leading: Icon(
                  Icons.local_library_outlined,
                  color: Theme.of(context).iconTheme.color,
                ),
                onTap: () => launchUrl(
                  Uri.parse('https://www.buymeacoffee.com/yzooniee'),
                ),
              ),
            SettingsTile(
              title: appLocalizations.aboutClima_libraries,
              subtitle: appLocalizations.aboutClima_libraries_subtitle,
              leading: Icon(
                Icons.source_outlined,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () {
                showLicensePage(
                  context: context,
                  applicationName: appLocalizations.appName,
                  applicationVersion: appVersion,
                );
              },
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_feedback,
              subtitle: appLocalizations.aboutClima_feedback_subtitle,
              leading: Icon(
                Icons.help_outline,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (context) => const HelpAndFeedbackDialog(),
                );
              },
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_sourceCode,
              subtitle: appLocalizations.aboutClima_sourceCode_subtitle,
              isThreeLine: true,
              leading: Icon(
                Icons.code,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () => launchUrl(
                Uri.parse('https://codeberg.org/Lacerte/clima'),
              ),
            ),
            SettingsTile(
              title: appLocalizations.aboutClima_credits,
              leading: Icon(
                Icons.attribution_outlined,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (context) => const CreditsDialog(),
                );
              },
            ),
            const SettingsDivider(),
          ],
        ),
      ),
    );
  }
}
